#!flask/bin/python

from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='qapanel',
    version='1.0',
    packages=find_packages(),
    install_requires = ['flask==0.9',
    'flask-login',
    'flask-openid',
    'flask-mail',
    'sqlalchemy==0.7.9',
    'flask-sqlalchemy==0.16',
    'sqlalchemy-migrate',
    'flask-whooshalchemy==0.54a',
    'flask-wtf==0.8.4',
    'pytz==2013b',
    'flask-babel==0.8',
    'flup',
    'flask-bootstrap==3.3.7.1',
    'psycopg2==2.7.3.2',
    'asn1crypto==0.23.0',
    'bcrypt==3.1.4',
    'cffi==1.11.2',
    'cryptography==2.1.2',
    'enum34==1.1.6',
    'idna==2.6',
    'ipaddress==1.0.18',
    'paramiko==2.3.1',
    'pyasn1==0.3.7',
    'pycparser==2.18',
    'pynacl==1.1.2',
    'enum==0.4.6',
    'marathon==0.9.3'
     ]
)

