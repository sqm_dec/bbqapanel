#!flask/bin/python
from app import app
from gevent.pywsgi import WSGIServer
from gevent import monkey
monkey.patch_all()
app.debug = True
http_server = WSGIServer(
    listener=('0.0.0.0', 5000),
    application=app.wsgi_app
)

http_server.serve_forever()

http_server.serve_forever()