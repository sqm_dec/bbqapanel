import logging, threading, time
from app import models
from Dashboard import Dashboard
from config import update_time

class Updater(threading.Thread):
    def  __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        logging.info("Start Thread of UPDATE info about servers")
        while True:
            time.sleep(int(update_time))
            try:
                db_servers = models.Server.query.join(models.server_types,
                                                      models.Server.server_type == models.server_types.server_type).add_columns(
                    models.Server.host, models.Server.server_type, models.Server.db_port, models.Server.db_user,
                    models.Server.db_password, models.Server.ssh_port, models.Server.ssh_user, models.Server.ssh_password,
                    models.server_types.db_build_type, models.server_types.server_build_type).all()
                types = []
                threads = []
                #START THREADS
                for i in db_servers:
                    if (types.count(i.server_type) == 0):
                        types.append(i.server_type)
                    t = Dashboard(i)
                    t.daemon = True
                    t.start()
                    threads.append(t)
                #WAIT WHERE INFO WILL UPDATE
                for t in threads:
                    t.join()
                logging.info("Dashboard inforamtion update FINISHED")
            except Exception,e:
                logging.info("Dashboard inforamtion update FAILED")
                logging.error(e)
