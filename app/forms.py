from flask_wtf import Form
from wtforms import StringField,validators, IntegerField, SelectField,BooleanField
from models import server_types,Server


class CreateServerForm(Form):
    types_array = []
    for tp in server_types.query.all():
        types_array.append((tp.server_type, tp.server_type))
    host = StringField('host',[validators.Length(min=1, max=120)])
    server_type = SelectField('server_type', choices=types_array)
    db_port = IntegerField('db_port',[validators.Length(min=0, max=5)])
    db_user = StringField('db_user',[validators.Length(min=1, max=120)])
    db_password = StringField('db_password',[validators.Length(min=1, max=120)])
    ssh_user = StringField('ssh_user',[validators.Length(min=1, max=120)])
    ssh_password = StringField('ssh_password',[validators.Length(min=1, max=120)])
    ssh_port = IntegerField('ssh_port',[validators.Length(min=0, max=5)])

class CreateServerTypeForm(Form):
    server_type = StringField('server_type',[validators.Length(min=1, max=20)])
    description = StringField('description', [validators.Length(min=1, max=120)])
    db_build_type = StringField('db_build_type', [validators.Length(min=1, max=120)])
    server_build_type = StringField('server_build_type', [validators.Length(min=1, max=120)])

class ShowDatabaseInfo(Form):
    host = SelectField('host', choices=[])

class SystemUser(Form):
    login = StringField('login',[validators.Length(min=1, max=120)])
    password = StringField('password',[validators.Length(min=1, max=120)])
    name = StringField('name',[validators.Length(min=1, max=120)])

class CreateRent(Form):
    user_login = SelectField('user_login', choices=[])
    server_host = SelectField('server_host', choices=[])