import os
basedir = os.path.abspath(os.path.dirname(__file__))


SQLALCHEMY_DATABASE_URI = 'postgresql://'+os.environ['DB_USER']+':'+os.environ['DB_PASSWORD']+'@'+os.environ['DB_HOST']+'/'+os.environ['DB_NAME']
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

marathon_port = os.environ['MARATHON_PORT']
marathon_proto = os.environ['MARATHON_PROTO']
mesos_port = os.environ['MESOS_PORT']
mesos_port_service = os.environ['MESOS_PORT_SERVICE']
mesos_proto = os.environ['MESOS_PROTO']
teamcity = {
    'user':os.environ['TM_USER'],
    'password':os.environ['TM_PASSWORD'],
    'host':os.environ['TM_HOST'],
    'port':os.environ['TM_PORT']
}
update_time = os.environ['UPDATE_TIME']