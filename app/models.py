from app import db, lm
import enum
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship


class server_types(db.Model):
    server_type = db.Column(db.String(20), index = True, unique = True, primary_key = True)
    description = db.Column(db.String(120))
    servers = relationship('Server',backref='server_server_type',lazy='dynamic')
    server_build_type = db.Column(db.String(120))
    db_build_type = db.Column(db.String(120))
    def __repr__(self):
        return "{'server_type':%s,'description':%s}"%(self.server_type,self.description)

class Server(db.Model):
    host = db.Column(db.String(120), index = True, unique = True, primary_key = True)
    server_type = db.Column(db.String(20),db.ForeignKey("server_types.server_type"), nullable=False)
    db_port = db.Column(db.Integer)
    db_user = db.Column(db.String(120))
    db_password = db.Column(db.String(120))
    ssh_user = db.Column(db.String(120))
    ssh_password = db.Column(db.String(120))
    ssh_port = db.Column(db.Integer)
    date_update = db.Column(db.DateTime())
    data_info = db.Column(JSON)
    users = relationship('servers_users', backref='servers_users_server_host',lazy='dynamic', primaryjoin="Server.host == servers_users.server_host")
    def __repr__(self):
        return "{'host':%s,'server_type':%s,'db_port':%s,'db_user':%s,'db_password':%s,'ssh_user':%s,'ssh_password':%s, 'ssh_port':%s}"%(self.host,self.server_type, str(self.db_port), self.db_user,self.db_password, self.ssh_user, self.ssh_password,str(self.ssh_port))



class system_users(db.Model):
    login = db.Column(db.String(120), index = True, unique = True, primary_key = True)
    password = db.Column(db.String(120))
    name = db.Column(db.String(120))
    is_admin = db.Column(db.Boolean)
    is_active = db.Column(db.Boolean)
    servers = relationship('servers_users', backref='servers_users_user_login',lazy='dynamic', primaryjoin="system_users.login == servers_users.user_login")
    def __repr__(self):
        return "{'login':%s,'password':%s,'name':%s,'is_admin':%s,'is_active':%s}"%(self.login,self.password,self.name,self.is_admin,self.is_active)
    def get_id(self):
        return self.login
    def is_authenticated(self):
        return True
    def is_anonymous(self):
        return False

class servers_users(db.Model):
    user_login = db.Column(db.String(120),db.ForeignKey("system_users.login"), nullable=False, primary_key = True)
    server_host = db.Column(db.String(120),db.ForeignKey("server.host"), nullable=False, primary_key = True)

@lm.user_loader
def load_user(userid):
    return system_users.query.filter_by(login=userid).first()
