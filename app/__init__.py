from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
import config
from flask_login import LoginManager


app = Flask(__name__)
app.config.from_object(config)
Bootstrap(app)
db = SQLAlchemy(app)
lm = LoginManager()
lm.init_app(app)
lm.login_view = '/index'

from updater import Updater

updater_thread = Updater()
updater_thread.start()


from app import views, models

