from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
server_types = Table('server_types', post_meta,
    Column('server_type', String(length=20), primary_key=True, nullable=False),
    Column('description', String(length=120)),
    Column('server_build_type', String(length=120)),
    Column('db_build_type', String(length=120)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['server_types'].columns['db_build_type'].create()
    post_meta.tables['server_types'].columns['server_build_type'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['server_types'].columns['db_build_type'].drop()
    post_meta.tables['server_types'].columns['server_build_type'].drop()
