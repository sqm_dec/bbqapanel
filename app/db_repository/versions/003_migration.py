from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
users = Table('users', pre_meta,
    Column('login', VARCHAR(length=120), primary_key=True, nullable=False),
    Column('password', VARCHAR(length=120)),
    Column('name', VARCHAR(length=120)),
    Column('is_admin', BOOLEAN),
    Column('is_block', BOOLEAN),
)

system_users = Table('system_users', post_meta,
    Column('login', String(length=120), primary_key=True, nullable=False),
    Column('password', String(length=120)),
    Column('name', String(length=120)),
    Column('is_admin', Boolean),
    Column('is_block', Boolean),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['users'].drop()
    post_meta.tables['system_users'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['users'].create()
    post_meta.tables['system_users'].drop()
