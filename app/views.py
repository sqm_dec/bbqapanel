import Queue, requests, json, logging, pylibmc, psycopg2
from flask import render_template,flash,redirect,request,session,g
from flask_login import login_user,current_user,logout_user
from hashlib import md5
from app import app,db,models,lm
from config import marathon_port,marathon_proto,mesos_port,mesos_port_service,mesos_proto
from forms import CreateServerForm, CreateServerTypeForm,ShowDatabaseInfo,SystemUser,CreateRent
from Dashboard import Dashboard
from memcached_stats import MemcachedStats

app.secret_key = "development-key"

@app.before_request
def before_request():
    g.user = current_user

@app.route('/auth',methods=['GET','POST'])
def auth_user():
    if (g.user.is_authenticated != False):
        return redirect('/index')
    params = request.form
    user = models.system_users.query.filter_by(login=params['login'], password=md5(params['password']).hexdigest()).first()
    if user == None:
        flash('Login Or Password is WRONG!!!','danger')
    else:
        login_user(user)
        flash('SuccessFully Auth', 'success')
    return render_template("dashboard.html", is_auth=g.user)

@app.route('/logout')
def logout():
    logout_user()
    return redirect('/index')

@app.route('/')
@app.route('/index')
def dashboard():
    return render_template("dashboard.html", is_auth = g.user)

@app.route('/dashboard/servers_qa_update')
def servers_qa_update():
    db_servers = models.Server.query.all()
    types = []
    servers = []
    for i in db_servers:
        if (types.count(i.server_type) == 0):
            types.append(str(i.server_type))
        if i.data_info != None:
            rent = models.servers_users.query.filter_by(server_host=i.host).first()
            if (rent == None):
                usedby = 'FREE'
            else:
                user = models.system_users.query.filter_by(login=rent.user_login).first()
                usedby = user.name
            servers.append({'info':i.data_info,'used':usedby})
    types.sort()
    return render_template("qa_servers.html", types = types, servers = servers)

#PROFILE
@app.route('/profile', methods=['GET'])
def profile():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    user_login = g.user.get_id()
    print user_login
    user = models.system_users.query.filter_by(login=user_login).first()
    form = SystemUser(request.form)
    return render_template("profile.html", is_auth = g.user, title='BBQApanel', user=user, form=form)

@app.route('/profile/save', methods=['GET','POST'])
def profile_save():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    if ((params['password'] == '') or (params['name'] == '')):
        flash('Not enought params to update User', 'danger')
    else:
        try:
            user_login = params['login']
            user = models.system_users.query.filter_by(login=user_login).update({'password':md5(params['password']).hexdigest(), 'name':params['name']})
            db.session.commit()
            flash('User successfylly updated', 'success')
        except:
            flash('Intrenal Server Error - Check your user parametres', 'danger')
    return redirect('/profile')


#DATABASE
@app.route('/database')
def index():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("index.html", is_auth = g.user, title = 'BBQApanel', form=form)

@app.route('/database/search', methods=['GET','POST'])
def database_info():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    database_req = models.Server.query.filter_by(host = params['host']).first()
    try:
        pcon = psycopg2.connect("host=%s port=%s dbname=postgres user=%s password=%s connect_timeout=3" % (database_req.host, database_req.db_port, database_req.db_user, database_req.db_password))
        pcur_test = pcon.cursor()
        pcur_test.execute(
            "SELECT datname,(pg_stat_file('base/'||oid ||'/PG_VERSION')).modification FROM pg_database WHERE datistemplate = false and datname <> 'postgres'")
        dbs = pcur_test.fetchall()
        pcon.close()
    except:
        pcon.close()
        dbs = []
    dbvs = []
    for db in dbs:
        try:
            pcon = psycopg2.connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=3"%(database_req.host,database_req.db_port,db[0],database_req.db_user,database_req.db_password))
            pcur_test = pcon.cursor()
            pcur_test.execute("show deploy.version")
            version = pcur_test.fetchall()[0][0]
            pcon.close()
        except:
            pcon.close()
            version = None
        dbvs.append({'dbname': db[0], 'version': version, 'last_update': db[1],'url': '/database/db?dbID=' + db[0] + '&host=' + params['host']})
    usrs = []
    try:
        pcon = psycopg2.connect("host=%s port=%s dbname=postgres user=%s password=%s connect_timeout=3" % (database_req.host, database_req.db_port, database_req.db_user, database_req.db_password))
        pcur_test = pcon.cursor()
        pcur_test.execute(
            "SELECT usename FROM pg_catalog.pg_user where usename <> 'postgres'")
        for us in  pcur_test.fetchall():
            usrs.append({'username':us[0]})
        pcon.close()
    except:
        pcon.close()
        usrs = []
    statics = []
    try:
        pcon = psycopg2.connect("host=%s port=%s dbname=postgres user=%s password=%s connect_timeout=3" % (database_req.host, database_req.db_port, database_req.db_user, database_req.db_password))
        pcur_test = pcon.cursor()
        pcur_test.execute(
            "select usename,client_addr,waiting,state,query from pg_stat_activity")
        for st in  pcur_test.fetchall():
            statics.append({'usename':st[0],'client_addr':st[1],'waiting':st[2],'state':st[3],'query':st[4]})
        pcon.close()
    except:
        pcon.close()
        statics = []
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    if len(dbvs) < 1:
        success = False
    else:
        success = True
    return render_template("database_search.html", is_auth = g.user, title='BBQApanel', success=success, form=form,dbvs=dbvs, usrs=usrs, host=params['host'],statics=statics)

@app.route('/database/db', methods=['GET'])
def db_database():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    host = request.args.get('host')
    dbID = request.args.get('dbID')
    tbs = []
    database_req = models.Server.query.filter_by(host=host).first()
    pcon = psycopg2.connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=3" % (
    database_req.host, database_req.db_port, dbID, database_req.db_user, database_req.db_password))
    pcur_test = pcon.cursor()
    pcur_test.execute(
        "SELECT distinct(table_schema) FROM information_schema.tables WHERE table_catalog = '%s' and table_schema not in ('information_schema','pg_catalog')" % (
            dbID))
    schemas = pcur_test.fetchall()
    for schema in schemas :
        pcur_test.execute(
            "select table_name, string_agg(concat('COLUMN NAME: ', column_name,' DATE TYPE: ',data_type,' MAY BE NULL: ',is_nullable),',') from information_schema.columns where table_schema='%s' group by table_name;" % (schema))
        tables = pcur_test.fetchall()
        tb_insch = []
        col_count = 0
        for table in tables:
            columns = []
            for col_var in table[1].split(','):
                columns.append(col_var)
            col_count = col_count + len(columns)
            tb_insch.append({'table':table[0], 'columns':columns})
        tbs.append({'schema': schema[0],'count':col_count, 'tables':tb_insch})
    pcur_test.execute(
        "SELECT pg_type.typname AS enumtype, string_agg(pg_enum.enumlabel,',') AS enumlabel FROM pg_type JOIN pg_enum ON pg_enum.enumtypid = pg_type.oid group by enumtype")
    etypes = []
    for tp in pcur_test.fetchall():
        print tp[1]
        etypes.append({'name': tp[0],'val': tp[1]})
    pcon.close()
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices = host_array
    return render_template("database.html", is_auth = g.user, title='BBQApanel', db_name=dbID,host=host, tbs = tbs, etypes = etypes)


#SITE
@app.route('/site')
def site():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("site.html", is_auth = g.user, title = 'BBQApanel', form=form)

@app.route('/site/search', methods=['GET','POST'])
def site_info():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    grp_lst = []
    try:
        grps = requests.get(marathon_proto + '://%s:%s/v2/groups' % (params['host'], str(marathon_port)))
        grp_list = grps.json()
        for group in grp_list['groups']:
            gr_id = group['id']
            app_lst = []
            for app in group['apps']:
                envs = {}
                stat = 'default'
                cpus = 0
                for key in app.keys():
                    if (key == 'env'):
                        envs_m = app['env']
                        envs = sorted(envs_m.items())
                    if (key == 'instances'):
                        if (app['instances'] < 1):
                            stat = 'danger'
                        else:
                            stat='success'
                    if (key == 'cpus'):
                        cpus = app['cpus']
                    if (key == 'version'):
                        version = app['version']
                try:
                    image = app['container']['docker']['image']
                except:
                    image = "No info"
                try:
                    version = app['version']
                except:
                    version = "No info"
                url = '/site/application?appID=' + app['id'] + '&host=' + params['host']
                app_lst.append({'id':app['id'].split('/')[2],'status': stat, 'cpu':cpus, 'image':image, 'env':envs, 'lastDeploy':version, 'url':url})
            success = True
            grp_lst.append({'grp_id':gr_id, 'apps':app_lst})
    except Exception,e:
        logging.error(e)
        success = False
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("site_search.html", title='BBQApanel', is_auth = g.user, success=success, form=form, grps = grp_lst, host=params['host'])

@app.route('/site/application', methods=['GET'])
def site_application():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    host = request.args.get('host')
    app_name = request.args.get('appID')
    envs = None
    app =  requests.get(marathon_proto+'://%s:%s/v2/apps%s'%(host,str(marathon_port), app_name))
    app_info = app.json()
    tasks = []
    for task in app_info['app']['tasks']:
        data_tsk = '{"type":"GET_TASKS"}'
        headers_tsk = {'Content-Type': 'application/json'}
        mesos_info = requests.post(mesos_proto+'://' + host + ':'+str(mesos_port)+'/api/v1', data=data_tsk, headers=headers_tsk)
        for ts in mesos_info.json()['get_tasks']['tasks']:
            if ts['task_id']['value'] == task['id']:
                framework_id = ts['framework_id']['value']
                slave_id = ts['agent_id']['value']
                data = '{"type":"GET_CONTAINERS"}'
                headers = {'Content-Type': 'application/json'}
                containers = requests.post(mesos_proto+'://' + host + ':'+str(mesos_port_service)+'/api/v1', data=data, headers=headers)
                for container in containers.json()['get_containers']['containers']:
                    if container['executor_id']['value'] == task['id']:
                        std_err = mesos_proto+"://"+host+":"+str(mesos_port_service)+"/files/read?path=/var/lib/mesos/slaves/"+slave_id+"/frameworks/"+framework_id+"/executors/"+task['id']+"/runs/"+container['container_id']['value']+"/stderr&length=50000"
                        std_out = mesos_proto+"://"+host+":"+str(mesos_port_service)+"/files/read?path=/var/lib/mesos/slaves/"+slave_id+"/frameworks/"+framework_id+"/executors/"+task['id']+"/runs/"+container['container_id']['value']+"/stdout&length=50000"
                        tasks.append({'task_id':task['id'],'std_out':std_out,'std_err':std_err, 'status':task['state']})

    try:
        envs = json.dumps(app_info['app']['env'], sort_keys=True)
    except:
        envs = {}
    try:
        cpu = app_info['app']['cpus']
    except:
        cpu = 0.1
    try:
        image = json.dumps(app_info['app']['container'])
    except:
        image = ""
    running = {"a_tasks":app_info['app']['tasksRunning'], "a_inst":app_info['app']['instances']}
    try:
        failed_task = {'state':True,'msg':app_info['app']['lastTaskFailure']['message'], 'ts':app_info['app']['lastTaskFailure']['timestamp']}
    except:
        failed_task = {'state': False}
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("site_application.html", is_auth = g.user, title='BBQApanel', failed_task = failed_task, running=running,  form=form, envs = envs, cpu=cpu, tasks = tasks,image=image, app_name=app_name,host=host)

@app.route('/application/save', methods=['GET','POST'])
def save_application():
    params = request.form
    host = params['host']
    app_name = params['AppId']
    js_envs = params['env_area']
    cpu = params['cpu']
    image = params['image']
    try:
        app_info = {"cpus": float(cpu), "env": json.loads(js_envs), "container": json.loads(image)}
        data = json.dumps(app_info)
        params = {'force':False}
        result = requests.put(marathon_proto+'://%s:%s/v2/apps%s'%(host,str(marathon_port), app_name), params=params, data=data)
        result_info = result.json()
        if (result.status_code == 200):
            flash('App is updated with new ENVS - Deploy: %s Version: %s' % (result_info['deploymentId'],result_info['version']), 'success')
        else:
            if (result.status_code == 422):
                flash('App Update is FAILED with message: %s %s  PATH:%s ERROR:%s ' % (result.status_code, result_info['message'], result_info['details'][0]['path'], result_info['details'][0]['errors']), 'danger')
            else:
                flash('App Update is FAILED with message: %s %s ' % (result.status_code, result_info['message']), 'danger')

    except Exception,e:
        logging.error(e)
        flash('App Update is FAILED: HOST IS UNAVALIBLE OR WRONG PARAMETRES SEND', 'danger')
    return redirect('/site/application?appID=%s&host=%s'%(app_name,host))


@app.route('/application/suspend', methods=['GET'])
def suspend_application():
    host = request.args.get('host')
    app_name = request.args.get('app_id')
    try:
        app_info = {"instances":0}
        data = json.dumps(app_info)
        params = {'force':True}
        result = requests.put(marathon_proto+'://%s:%s/v2/apps%s'%(host,str(marathon_port), app_name), params=params, data=data)
        result_info = result.json()
        if (result.status_code == 200):
            flash('App is SUSPENDED', 'success')
        else:
            if (result.status_code == 422):
                flash('App Suspend is FAILED with message: %s %s  PATH:%s ERROR:%s ' % (result.status_code, result_info['message'], result_info['details'][0]['path'], result_info['details'][0]['errors']), 'danger')
            else:
                flash('App Suspend is FAILED with message: %s %s ' % (result.status_code, result_info['message']), 'danger')

    except Exception,e:
        logging.error(e)
        flash('App Suspend is FAILED: HOST IS UNAVALIBLE OR WRONG PARAMETRES SEND', 'danger')
    return redirect('/site/application?appID=%s&host=%s'%(app_name,host))

@app.route('/application/start', methods=['GET'])
def start_application():
    host = request.args.get('host')
    app_name = request.args.get('app_id')
    try:
        app_info = {"instances":1}
        data = json.dumps(app_info)
        params = {'force':False}
        result = requests.put(marathon_proto+'://%s:%s/v2/apps%s'%(host,str(marathon_port), app_name), params=params, data=data)
        result_info = result.json()
        if (result.status_code == 200):
            flash('App is RUNNING', 'success')
        else:
            if (result.status_code == 422):
                flash('App Start is FAILED with message: %s %s  PATH:%s ERROR:%s ' % (result.status_code, result_info['message'], result_info['details'][0]['path'], result_info['details'][0]['errors']), 'danger')
            else:
                flash('App Start is FAILED with message: %s %s ' % (result.status_code, result_info['message']), 'danger')

    except Exception,e:
        logging.error(e)
        flash('App Suspend is FAILED: HOST IS UNAVALIBLE OR WRONG PARAMETRES SEND', 'danger')
    return redirect('/site/application?appID=%s&host=%s'%(app_name,host))

@app.route('/site/app_logs', methods=['GET','POST'])
def get_log():
    params = request.form
    url = params['url']+'&offset='+params['offset']
    response = requests.get(url)
    info = response.json()['data']
    return info

#MEMCASHED
#SITE
@app.route('/memcashed')
def memcashed():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("memcashed.html", is_auth = g.user, title = 'BBQApanel', form=form)

@app.route('/memcashed/connect', methods=['GET','POST'])
def memcashed_connect():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    mem = MemcachedStats(params['host'], '11211')
    mem_keys = []
    try:
        for key in mem.keys():
            if key not in mem_keys:
                mem_keys.append(key)
        success = True
    except:
        mem_keys = []
        success = True
    form = ShowDatabaseInfo(request.form)
    host_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    form.host.choices=host_array
    return render_template("memcashed_client.html", title='BBQApanel', is_auth = g.user, form=form, mem_keys=mem_keys,success=success, host=params['host'])


@app.route('/memcashed/get_val', methods=['GET','POST'])
def memcashed_get():
    params = request.form
    try:
        mc = pylibmc.Client([params['host']], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        val = mc.get(str(params['value']))
    except Exception, e:
        logging.error(e)
        val = ''
    return val

@app.route('/memcashed/set_val', methods=['GET','POST'])
def memcashed_set():
    params = request.form
    try:
        mc = pylibmc.Client([params['host']], binary=True, behaviors={"tcp_nodelay": True, "ketama": True})
        mc.set(str(params['key']), str(params['val']))
        val = mc.get(str(params['key']))
    except Exception, e:
        logging.error(e)
        val = ''
    return val

#SERVERS ROUTES
@app.route('/servers')
def servers():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    servers = models.Server.query.all()
    form = CreateServerForm(request.form)
    return render_template("servers.html", is_auth = g.user, title = 'BBQApanel', servers = servers, form = form)

@app.route('/servers/create', methods=['GET','POST'])
def servers_create():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    param_with_none =0
    for param in params:
        if (len(params[param]) < 1):
            param_with_none += 1
    if (param_with_none > 1):
        flash('Not enought params to create Server', 'danger')
    else:
        try:
            new_server = models.Server(host = params['host'], server_type = params['server_type'],db_port = int(params['db_port']),db_user = params['db_user'],db_password = params['db_password'], ssh_port = int(params['ssh_port']), ssh_user = params['ssh_user'],ssh_password = params['ssh_password'])
            db.session.add(new_server)
            db.session.commit()
            flash('Server successfylly created', 'success')
        except:
            flash('Intrenal Server Error - Check your server parametres', 'danger')
    return redirect('/servers')

@app.route('/servers/delete', methods=['GET'])
def servers_delete():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    try:
        server_host = request.args.get('host')
        server = models.Server.query.filter_by(host=server_host).first()
        db.session.delete(server)
        db.session.commit()
        flash('Server successfylly deleted', 'success')
    except:
        flash('Intrenal Server Error', 'danger')
    return redirect('/servers')

@app.route('/servers/update', methods=['GET'])
def servers_update():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    server_host = request.args.get('host')
    server = models.Server.query.filter_by(host=server_host).first()
    form = CreateServerForm(request.form)
    return render_template("servers_update.html", is_auth = g.user, title='BBQApanel', server=server, form=form)

@app.route('/servers/save', methods=['GET','POST'])
def servers_save():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    param_with_none =0
    for param in params:
        if (len(params[param]) < 1):
            param_with_none += 1
    if (param_with_none > 1):
        flash('Not enought params to update Server', 'danger')
    else:
        try:
            server_host = params['host']
            server = models.Server.query.filter_by(host=server_host).update({'server_type': params['server_type'], 'db_port':int(params['db_port']), 'db_user':params['db_user'], 'db_password':params['db_password'], 'ssh_port':int(params['ssh_port']), 'ssh_user':params['ssh_user'],'ssh_password':params['ssh_password']})
            db.session.commit()
            flash('Server successfylly updated', 'success')
        except:
            flash('Intrenal Server Error - Check your server parametres', 'danger')
    return redirect('/servers')

#SERVER TYPES ROUTES
@app.route('/server_types')
def server_types():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    server_types = models.server_types.query.all()
    form = CreateServerTypeForm(request.form)
    return render_template("server_types.html", is_auth = g.user, title = 'BBQApanel', server_types = server_types, form = form)

@app.route('/server_types/create', methods=['GET','POST'])
def server_types_create():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    if (params['server_type'] == ''):
        flash('Server type name Not Found. Check your input params.', 'danger')
    else:
        try:
            new_type = models.server_types(server_type = params['server_type'], description = params['description'], db_build_type = params['db_build_type'], server_build_type = params['server_build_type'])
            db.session.add(new_type)
            db.session.commit()
            flash('Server type successfylly created', 'success')
        except:
            flash('Intrenal Server Error - Check your params', 'danger')
    return redirect('/server_types')

@app.route('/server_types/update', methods=['GET'])
def server_types_update():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    server_type = request.args.get('server_type')
    serv_type = models.server_types.query.filter_by(server_type=server_type).first()
    form = CreateServerTypeForm(request.form)
    return render_template("server_types_update.html", is_auth = g.user, title='BBQApanel', server_type=serv_type, form=form)

@app.route('/server_types/save', methods=['GET','POST'])
def server_types_save():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    try:
        st = params['server_type']
        server = models.server_types.query.filter_by(server_type=st).update({'description':params['description'], 'db_build_type':params['db_build_type'], 'server_build_type':params['server_build_type']})
        db.session.commit()
        flash('Server type successfylly updated', 'success')
    except:
        flash('Intrenal Server Error - Check your params parametres', 'danger')
    return redirect('/server_types')

@app.route('/server_types/delete', methods=['GET'])
def server_types_delete():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    try:
        server_type = request.args.get('server_type')
        servers_with_type = models.Server.query.filter_by(server_type=server_type).all()
        if len(servers_with_type) > 0:
            flash('This type contain %d Servers. Please change type for there and try again this operation.'%(len(servers_with_type)), 'danger')
        else:
            server_types = models.server_types.query.filter_by(server_type=server_type).first()
            db.session.delete(server_types)
            db.session.commit()
            flash('Server type successfylly deleted', 'success')
    except:
            flash('Intrenal Server Error', 'danger')
    return redirect('/server_types')


#USERS
@app.route('/users')
def users():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    users = models.system_users.query.all()
    form = SystemUser(request.form)
    return render_template("users.html", is_auth = g.user, title = 'BBQApanel', users = users, form = form)

@app.route('/users/create', methods=['GET','POST'])
def users_create():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    param_with_none =0
    for param in params:
        if (len(params[param]) < 1):
            param_with_none += 1
    if (param_with_none > 1):
        flash('Not enought params to create User', 'danger')
    else:
        try:
            new_user = models.system_users(login = params['login'], password = md5(params['password']).hexdigest(),name = params['name'], is_active = True)
            db.session.add(new_user)
            db.session.commit()
            flash('User successfully created', 'success')
        except:
            flash('Intrenal Server Error - Check your user parametres', 'danger')
    return redirect('/users')

@app.route('/users/delete', methods=['GET'])
def users_delete():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    try:
        user_login = request.args.get('login')
        user = models.system_users.query.filter_by(login=user_login).first()
        db.session.delete(user)
        db.session.commit()
        flash('User successfylly deleted', 'success')
    except:
        flash('Intrenal Server Error', 'danger')
    return redirect('/users')

@app.route('/users/update', methods=['GET'])
def users_update():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    user_login = request.args.get('login')
    user = models.system_users.query.filter_by(login=user_login).first()
    form = SystemUser(request.form)
    return render_template("users_update.html", is_auth = g.user, title='BBQApanel', user=user, form=form)

@app.route('/users/save', methods=['GET','POST'])
def users_save():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    if ((params['password'] == '') or (params['name'] == '')):
        flash('Not enought params to update User', 'danger')
    else:
        try:
            user_login = params['login']
            user = models.system_users.query.filter_by(login=user_login).update({'password':md5(params['password']).hexdigest(), 'name':params['name']})
            db.session.commit()
            flash('User successfylly updated', 'success')
        except:
            flash('Intrenal Server Error - Check your user parametres', 'danger')
    return redirect('/users')

#SERVER RENT
@app.route('/server_rent')
def server_rent():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    server_users = models.servers_users.query.all()
    form = CreateRent(request.form)
    host_array = []
    users_array = []
    for tp in models.Server.query.all():
        host_array.append((tp.host, tp.host))
    for us in models.system_users.query.all():
        users_array.append((us.login, us.login))
    form.server_host.choices=host_array
    form.user_login.choices = users_array
    return render_template("server_rent.html", is_auth = g.user, title = 'BBQApanel', server_users = server_users, form = form)

@app.route('/server_rent/create', methods=['GET','POST'])
def server_rent_create():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    params = request.form
    if ((params['server_host'] == '') or  (params['user_login'] == '')):
        flash('Check your input params.', 'danger')
    else:
        check_rent = models.servers_users.query.filter_by(server_host=params['server_host']).first()
        if check_rent != None:
            flash('Server is busy! Select other server.', 'danger')
            return redirect('/server_rent')
        try:
            new_rent = models.servers_users(server_host = params['server_host'], user_login = params['user_login'])
            db.session.add(new_rent)
            db.session.commit()
            flash('Rent successfylly created', 'success')
        except:
            flash('Intrenal Server Error - Check your params', 'danger')
    return redirect('/server_rent')

@app.route('/server_rent/delete', methods=['GET'])
def server_rent_delete():
    if (g.user.is_authenticated == False):
        return redirect('/index')
    try:
        server_host = request.args.get('server_host')
        user_login = request.args.get('user_login')
        rent = models.servers_users.query.filter_by(server_host=server_host, user_login=user_login).first()
        db.session.delete(rent)
        db.session.commit()
        flash('Rent successfylly deleted', 'success')
    except:
        flash('Intrenal Server Error', 'danger')
    return redirect('/server_rent')