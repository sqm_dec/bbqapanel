#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
os.environ["NLS_LANG"] = ".UTF8"
import psycopg2, threading, json, requests, logging
from pyteamcity import TeamCity
from app import app,db,models
from config import marathon_port,teamcity, marathon_proto
from time import gmtime,strftime

reload(sys)
sys.setdefaultencoding('utf8')

class Dashboard(threading.Thread):
    def  __init__(self,server):
        threading.Thread.__init__(self)
        self.server = server
    def run(self):
        dbs = self.get_db_version('db_game')
        site_deploy = self.get_last_build(self.server.server_build_type)
        db_deploy = self.get_last_build(self.server.db_build_type)
        sitev = self.get_site_version('/online/site')
        site_status = self.get_site_status()
        if (dbs[1] == 'remove' or sitev[1] == 'remove' or site_status == 'remove'):
            status = 'red'
        else:
            status = 'green'
        try:
            srv_str = {'host': self.server.host, 'site_status': site_status, 'type': self.server.server_type,
                       'db_version': {'info': dbs[0], 'ico': dbs[1]}, 'db_deploy': db_deploy,
                       'site_deploy': site_deploy, 'site_version': {'info': sitev[0], 'ico': sitev[1]},
                       'status': status}
            server_inf = models.Server.query.filter_by(host=self.server.host).update(
            {'data_info': srv_str, 'date_update':strftime("%Y-%m-%d %H:%M:%S", gmtime())})
            db.session.commit()
            db.session.remove()
        except Exception,e:
            logging.error(e)

    def get_site_status(self):
        try:
            req = requests.get('http://'+self.server.host)
            if (req.status_code != 200):
                return 'remove'
            else:
                return 'ok'
        except:
            return 'remove'

    def get_db_version(self,dbname):
        try:
            pcon = psycopg2.connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=3"%(self.server.host,self.server.db_port,dbname,self.server.db_user,self.server.db_password))
            pcur_test = pcon.cursor()
            pcur_test.execute("show deploy.version")
            result = pcur_test.fetchall()[0][0],'ok'
            pcon.close()
            return result
        except:
            pcon.close()
            return None,'remove'

    def get_databases(self):
       try:
            pcon = psycopg2.connect("host=%s port=%s dbname=postgres user=%s password=%s connect_timeout=3"%(self.server.host,self.server.db_port,self.server.db_user,self.server.db_password))
            pcur_test = pcon.cursor()
            pcur_test.execute("SELECT datname,(pg_stat_file('base/'||oid ||'/PG_VERSION')).modification FROM pg_database WHERE datistemplate = false and datname <> 'postgres'")
            result = pcur_test.fetchall()
            pcon.close()
            return result
       except:
           pcon.close()
           return []

    def get_site_version(self,app_dir):
        try:
            app = requests.get(marathon_proto + '://%s:%s/v2/apps%s' % (self.server.host, str(marathon_port), app_dir))
            app_info = app.json()
            res = app_info['app']['container']['docker']['image'].split(':')[1],'ok'
        except Exception,e:
            res = None,'remove'
        return res

    def get_last_build(self,build_id):
        try:
            tc = TeamCity(teamcity['user'],teamcity['password'],teamcity['host'],int(teamcity['port']))
            find_result = False
            who = None
            for build in json.loads(json.dumps(tc.get_builds(build_type_id=build_id, count=1000)))['build']:
                for param in json.loads(json.dumps(tc.get_build_parameters_by_build_id(build_id = int(build['id']),count=1)))['property']:
                    if (param['name'] == 'SERVER'):
                        if (param['value'] == self.server.host):
                            find_result = True
                    if (param['name'] == 'teamcity.build.triggeredBy.username' and find_result == True):
                        who =  param['value']
                if (find_result == True):
                    finish_date = json.loads(json.dumps(tc.get_build_by_build_id(build_id=int(build['id']), count=1)))['finishDate'].split('+')[0].split('T')
                    ymd = finish_date[0][0]+finish_date[0][1]+finish_date[0][2]+finish_date[0][3]+'-'+finish_date[0][4]+finish_date[0][5]+'-'+finish_date[0][6]+finish_date[0][7]
                    hhmmss = finish_date[1][0]+finish_date[1][1]+':'+finish_date[1][2]+finish_date[1][3]+':'+finish_date[1][4]+finish_date[1][5]
                    return {'status':'ok','info':{'build_id' :build['number'], 'build_user':who,'build_result':build['status'],'build_url':build['webUrl'], 'finishDate':ymd+' '+hhmmss}}
            if (find_result == False):
                return {'status': 'warning-sign',
                        'info': {'build_id': 'Very Old Build', 'build_user': None, 'build_result': None,
                                 'build_url': None,'finishDate':'A long time ago'}}
        except:
            return {'status':'warning-sign', 'info':{'build_id' :None, 'build_user':None,'build_result':None,'build_url':'/','finishDate':None}}