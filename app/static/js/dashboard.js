    function show_servers()
    {
        $.ajax({
            url: "/dashboard/servers_qa_update",
            async: true,
            complete : function(){ $("#progress").hide() },
            success: function(html){
                $("#dashboard_qa").html(html);
            }
        });
    }

    $(document).ready(function(){
        show_servers();
        setInterval('show_servers()',60000);
    });