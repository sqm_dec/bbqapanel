function show_logs(id, offset)
{
    //var offset = $(id).val().length;
    $.ajax({
        type: 'POST',
        url: '/site/app_logs',
        data: {
        url: id,
        offset: offset
        },
        useDefaultXhrHeader: false,
        cache: true,
        success: function(result){
            document.getElementById(id).append(result);
            document.getElementById(id).scrollTop=document.getElementById(id).scrollHeight;
        }
    });
}

function start_logs()
{
    var std_inputs = document.getElementsByName("std_area");
    for (var i = 0; i < std_inputs.length; i++) {
        show_logs(std_inputs[i].id,std_inputs[i].value.length);
    }

    var err_inputs = document.getElementsByName("err_area");
    for (var i = 0; i < err_inputs.length; i++) {
        show_logs(err_inputs[i].id,err_inputs[i].value.length);
    }
}


$(document).ready(function(){
    start_logs();
    setInterval('start_logs()',5000);
    var myobj = JSON.parse(document.getElementById("env_area").value);
    var textedJson = JSON.stringify(myobj,undefined, 2);
    document.getElementById("env_area").value = textedJson;
    var myobj_cont = JSON.parse(document.getElementById("image").value);
    var textedJson_cont = JSON.stringify(myobj_cont,undefined, 2);
    document.getElementById("image").value = textedJson_cont;
    $('#envs').on('shown.bs.collapse', function () {
       $("#env_ico")
          .removeClass("glyphicon-plus")
          .addClass("glyphicon-minus");
     });

     $('#envs').on('hidden.bs.collapse', function () {
        $("#env_ico")
           .removeClass("glyphicon-minus")
           .addClass("glyphicon-plus");
     });
});