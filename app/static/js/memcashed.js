$(document).ready(function(){
$(".chosen-select").chosen();

 $('#bb').on('change', '#m_keys', function(){
    var value = $(this).val();
    var host = $('#host').val();
     $.ajax({
      type: "POST",
      url: "/memcashed/get_val",
      data: {value: value, host: host},
      success: function(date){
        $("#value_area").val(date);
      }
    });
  });

$('#bb').on('click', '#mem_save', function(){
    var host = $('#host').val();
    var key = $('#m_keys').val();
    var val = $('#value_area').val();
     $.ajax({
      type: "POST",
      url: "/memcashed/set_val",
      data: {key: key,val: val, host: host},
      success: function(date){
        $("#value_area").val(date);
      }
    });
  });


});