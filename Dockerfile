FROM python:2.7
MAINTAINER Yushankov Sergey "senys3528@mail.ru"
COPY . /app
WORKDIR /app
RUN apt-get update && apt-get install -y libmemcached-dev
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["run.py"]